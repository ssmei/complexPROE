CC=g++
C_FLAGS=-std=c++17 -Wall -Wextra
BIN=bin
SRC=src
INCLUDE=include
EXECUTABLE=complexPROE
TEST_EXE=test

all: $(BIN)/$(EXECUTABLE)

clean:
	$(RM) -r $(BIN)
	
run: all
	./$(BIN)/$(EXECUTABLE)
	
$(BIN)/$(EXECUTABLE): $(SRC)/*
	@mkdir -p $(BIN)
	$(CC) $(C_FLAGS) -I $(INCLUDE) $^ -o $@
	
