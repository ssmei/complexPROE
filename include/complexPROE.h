#pragma once
#include <string>
class ComplexPROE
{
    private:
        float re_;
        float im_;
    public:
        static ComplexPROE tempComplex;
        ComplexPROE();
        ComplexPROE(float re, float im=0);
        ComplexPROE(const ComplexPROE&);
        
        ComplexPROE& operator++();
        ComplexPROE operator++(int);
        ComplexPROE& operator--();
        ComplexPROE operator--(int);
        
        void displayData(std::string);
        ComplexPROE& operator=(const ComplexPROE& );
        ComplexPROE& operator+=(const ComplexPROE& );  // Added
        ComplexPROE& operator+=(int&);  // Added
        ComplexPROE& operator-=(const ComplexPROE& );  // Added
        ComplexPROE& operator-=(int&);  // Added
        
        friend bool operator==(const ComplexPROE&, const ComplexPROE&); // Added
        friend bool operator!=(const ComplexPROE&, const ComplexPROE&); // Added
        
        friend ComplexPROE& operator+(const ComplexPROE&, const ComplexPROE&);
        friend ComplexPROE& operator+(float&, const ComplexPROE&);  // Added
        friend ComplexPROE& operator-(const ComplexPROE&, const ComplexPROE&);  // Added
        friend ComplexPROE& operator-(float&, const ComplexPROE&);  // Added
        friend ComplexPROE& operator*(const ComplexPROE&, const ComplexPROE&);  // Added
        friend ComplexPROE& operator*(float&, const ComplexPROE&);  //Added
        
        friend ComplexPROE& operator/(const ComplexPROE&, const ComplexPROE&);  // Added
        friend ComplexPROE& operator/(float, const ComplexPROE&);  // Added
        friend ComplexPROE& operator/(const ComplexPROE&, float);  // Added
        
        
        friend ComplexPROE conjugated(const ComplexPROE);  // Added
        friend float absoluteValueSquare(const ComplexPROE);  // Added
        friend float absoluteValue(const ComplexPROE);
        friend std::ostream& operator<<(std::ostream &, ComplexPROE&);
        friend std::istream& operator>>(std::istream &, ComplexPROE&);
        
        void debugInfo(std::string, ComplexPROE *);
};
