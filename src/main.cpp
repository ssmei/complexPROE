#include <iostream>
#include "complexPROE.h"
#include "assert.h"

int main()
{
    ComplexPROE c1(1, 2);
    ComplexPROE t1(1, 2);
    
    assert (c1++ == t1);  // ==
    assert (c1 == t1+1);  // c1 is increased after being called once
    assert (!(c1 != t1+1));  // != 
    
    assert (c1-- == ++t1);
    assert (c1 == --t1);
    
    ComplexPROE c2_1(2, 1);
    ComplexPROE c2_2(1, 2);
    ComplexPROE t2(0, 5);
    
    assert ((c2_1*c2_2) == t2);  // Complex * Complex
    
    ComplexPROE c3_1(1,8);
    ComplexPROE c3_2(2,3);
    ComplexPROE t3(2,1);
    
    assert (c3_1/c3_2 == t3);  // Complex / Complex
    
    ComplexPROE c4(50, 50);
    ComplexPROE t4_1(5,5);
    
    assert (c4/10 == t4_1);  // Complex / float
    assert (c4*10 == 10*c4);
    
    ComplexPROE t4_2(500, 500);
    assert (c4*10== t4_2);  // Complex * float
    
    ComplexPROE t4_3(10, -10);
    assert (1000/c4 == t4_3);  // float / Complex

    
}
