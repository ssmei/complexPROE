#include "complexPROE.h"
#include <iostream>
#include <cmath>
ComplexPROE::ComplexPROE()
{
    re_=1.0; im_=0.0;
    //debugInfo("default constructor", this);
}

ComplexPROE::ComplexPROE(float re, float im)
{
    re_=re; im_=im;
    //debugInfo("2 parameters constructor", this);
}

ComplexPROE::ComplexPROE(const ComplexPROE& anotherComplex)
{
    if(this!=&anotherComplex)
    {
        re_=anotherComplex.re_;
        im_=anotherComplex.im_;
        //debugInfo("copy constructor", this);
    }
}

ComplexPROE& ComplexPROE::operator++()
{
    re_+=1.0;
    return *this;
}

ComplexPROE ComplexPROE::operator++(int)
{
    re_+=1.0;
    return ComplexPROE(re_-1.0,im_);
}

ComplexPROE& ComplexPROE::operator--()
{
    re_-=1.0;
    return *this;
}

ComplexPROE ComplexPROE::operator--(int)
{
    re_-=1.0;
    return ComplexPROE(re_+1.0,im_);
}

void ComplexPROE::displayData(std::string t)
{
    std::cout<<t<<": "<<"re="<<re_<<" im="<<im_<<std::endl;
}

ComplexPROE& ComplexPROE::operator=(const ComplexPROE& anotherComplex)
{
    if(this!=&anotherComplex)
    {
        re_=anotherComplex.re_;
        im_=anotherComplex.im_;
    }
    //debugInfo("operator =", this);
    return *this;
}

ComplexPROE& ComplexPROE::operator+=(const ComplexPROE& anotherComplex)
{
    re_ += anotherComplex.re_;
    im_ += anotherComplex.im_;
    return *this;
}

ComplexPROE& ComplexPROE::operator+=(int& val)
{
    re_ += val;
    return *this;
}

ComplexPROE& ComplexPROE::operator-=(int& val)
{
    re_ -= val;
    return *this;
}


ComplexPROE& ComplexPROE::operator-=(const ComplexPROE& anotherComplex)
{
    re_ -= anotherComplex.re_;
    im_ -= anotherComplex.im_;
    return *this;
}

ComplexPROE& operator+(const ComplexPROE& a, const ComplexPROE& b)
{
    ComplexPROE::tempComplex.re_=a.re_+b.re_;
    ComplexPROE::tempComplex.im_=a.im_+b.im_;
    return ComplexPROE::tempComplex;
}

ComplexPROE& operator+(float& a, const ComplexPROE& b)
{
    ComplexPROE::tempComplex.re_= b.re_ + a;
    return ComplexPROE::tempComplex;
}

ComplexPROE& operator-(const ComplexPROE& a, const ComplexPROE& b)
{
    ComplexPROE::tempComplex.re_ = a.re_ - b.re_;
    ComplexPROE::tempComplex.im_ = a.im_ - b.im_;
    return ComplexPROE::tempComplex;
}

ComplexPROE& operator-(float& a, const ComplexPROE& b)
{
    ComplexPROE::tempComplex.re_ = a - b.re_;
    ComplexPROE::tempComplex.im_ = b.im_;
    return ComplexPROE::tempComplex;
}

bool operator==(const ComplexPROE& a, const ComplexPROE& b)
{
    return (a.re_ == b.re_) && (a.im_ == b.im_);
}

bool operator!=(const ComplexPROE& a, const ComplexPROE& b)
{
    return !(a==b);
}


ComplexPROE& operator*(const ComplexPROE& a, const ComplexPROE& b)
{
    ComplexPROE::tempComplex.re_ = a.re_*b.re_ - a.im_*b.im_;
    ComplexPROE::tempComplex.im_ = a.re_*b.im_ + a.im_*b.re_;
    return ComplexPROE::tempComplex;
}

ComplexPROE& operator*(float& a, const ComplexPROE& b)
{
    ComplexPROE::tempComplex.re_ = b.re_ * a;
    ComplexPROE::tempComplex.im_ = b.im_ * a;
    return ComplexPROE::tempComplex;
}

ComplexPROE& operator/(const ComplexPROE& a, const ComplexPROE& b)
{
    float denominator = absoluteValueSquare(b);
    //std::cout<<"denominator="<<denominator<<std::endl;
    ComplexPROE::tempComplex = a * conjugated(b);
    ComplexPROE::tempComplex.re_ /= denominator;
    ComplexPROE::tempComplex.im_ /= denominator;
    return ComplexPROE::tempComplex;
}

ComplexPROE& operator/(const ComplexPROE& a, float denominator)
{
    return a * (1/denominator);
}

ComplexPROE& operator/(float numerator, const ComplexPROE& a)
{
    ComplexPROE temp;
    temp = conjugated(a);
    ComplexPROE::tempComplex = conjugated(a) / absoluteValueSquare(a);
    return numerator * ComplexPROE::tempComplex;
}

void ComplexPROE::debugInfo(std::string text, ComplexPROE * adres)
{
    std::cout<<text<<" -> object at:"<<std::hex<<adres<<std::endl;
}

ComplexPROE ComplexPROE::tempComplex;

std::ostream& operator<<(std::ostream & os, ComplexPROE& cp)
{
    if (cp.im_ < 0) os<<'('<<cp.re_<<cp.im_<<"j)";
    else os<<'('<<cp.re_<<"+"<<cp.im_<<"j)";
    return os;
}

std::istream& operator>>(std::istream & is, ComplexPROE& cp)
{
    is>>cp.re_;
    char c=is.get();
    if(c=='+' || c=='-')  is>>cp.im_;
    else is.unget();
    return is;
}

ComplexPROE conjugated(const ComplexPROE a)
{
    return ComplexPROE(a.re_, -a.im_);
}

float absoluteValueSquare(const ComplexPROE a)
{
    return (a.re_ * a.re_) + (a.im_ * a.im_);
}

float absoluteValue(const ComplexPROE a)
{
    return sqrt(absoluteValueSquare(a));
}
